export function fitDimensionsToSpace(
  itemWidth: number,
  itemHeight: number,
  containerWidth: number,
  containerHeight: number
): [number, number] {
  const itemRatio = itemWidth / itemHeight;

  if (containerHeight * itemRatio > containerWidth) {
    return [itemWidth / (itemHeight / containerHeight), containerHeight];
  } else {
    return [containerWidth, itemHeight / (itemWidth / containerWidth)];
  }
}
