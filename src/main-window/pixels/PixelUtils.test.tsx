import { emptyGrid, updatePixel, updatePixels } from "./PixelUtils";

const blackPixel = { r: 0, g: 0, b: 0 };
const whitePixel = { r: 255, g: 255, b: 255 };

describe("PixelUtils", () => {
  test("empty grid returns a 128x64 empty grid", () => {
    const grid = emptyGrid();

    expect(grid.length).toEqual(128);
    grid.forEach((col) => {
      expect(col.length).toEqual(64);
    });

    grid.forEach((col) => {
      col.forEach((pixel) => {
        expect(pixel).toEqual(blackPixel);
      });
    });
  });

  test("update pixel updates a single pixel", () => {
    const grid = updatePixel(emptyGrid(), 1, 2, whitePixel);

    grid.forEach((col, x) => {
      col.forEach((pixel, y) => {
        expect(pixel).toEqual(x === 1 && y === 2 ? whitePixel : blackPixel);
      });
    });
  });

  test("update pixels updates a multiple pixel", () => {
    const grid = updatePixels(emptyGrid(), [
      { x: 1, y: 2, colour: whitePixel },
      { x: 3, y: 4, colour: whitePixel },
    ]);

    grid.forEach((col, x) => {
      col.forEach((pixel, y) => {
        expect(pixel).toEqual(
          (x === 1 && y === 2) || (x === 3 && y === 4) ? whitePixel : blackPixel
        );
      });
    });
  });
});
