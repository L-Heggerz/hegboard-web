import { drawPixelCircle, drawPixelLine } from "./PaintingUtils";

const white = { r: 0, g: 0, b: 0 };

test("draws single pixel for circle of radius 0.5", () => {
  expect(drawPixelCircle({ x: 3, y: 3 }, 0.5, 1, white)).toEqual([
    {
      x: 3,
      y: 3,
      colour: white,
    },
  ]);
});

test("draws nine pixels for circle of radius 1.5", () => {
  expect(drawPixelCircle({ x: 3, y: 3 }, 1.5, 1, white)).toEqual(
    expect.arrayContaining([
      { x: 2, y: 2, colour: white },
      { x: 2, y: 3, colour: white },
      { x: 2, y: 4, colour: white },
      { x: 3, y: 2, colour: white },
      { x: 3, y: 3, colour: white },
      { x: 3, y: 4, colour: white },
      { x: 4, y: 2, colour: white },
      { x: 4, y: 3, colour: white },
      { x: 4, y: 4, colour: white },
    ])
  );
});

test.skip("draws single pixel line between two pixels", () => {
  expect(drawPixelLine({ x: 1, y: 3 }, { x: 3, y: 3 }, 1, 1, white)).toEqual(
    expect.arrayContaining([
      { x: 1, y: 3, colour: white },
      { x: 2, y: 3, colour: white },
      { x: 3, y: 3, colour: white },
    ])
  );
});
