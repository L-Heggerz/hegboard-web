import React from "react";
import StaticDrawing from "./drawing/StaticDrawing";
import { fitDimensionsToSpace } from "./utills";

export default function MainWindow(props: { fullscreen: boolean }) {
  const width = props.fullscreen ? window.innerWidth : window.innerWidth - 240;
  const height = window.innerHeight - 64;

  if (width < 128 || height < 64) {
    const [imgX, imgY] = fitDimensionsToSpace(930, 573, width, height);
    return (
      <img
        src={process.env.PUBLIC_URL + "/toosmol.png"}
        alt={"smol"}
        width={imgX}
        height={imgY}
      />
    );
  }

  return <StaticDrawing />;
}
