import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import Hegboard from "./Hegboard";

ReactDOM.render(
  <React.StrictMode>
    <div className="HegBoard">
      <Hegboard />
    </div>
  </React.StrictMode>,
  document.getElementById("root")
);
